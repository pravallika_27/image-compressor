from tkinter import *
import tkinter.messagebox as tkMessageBox
import tkinter as tk
from tkinter import messagebox
from PIL import Image, ImageTk
import re
import sqlite3
import PIL
from tkinter.filedialog import askopenfilename, asksaveasfilename

root = Tk()
root.title("Image Compressor")
width = 640
height = 480
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()
x = (screen_width / 2) - (width / 2)
y = (screen_height / 2) - (height / 2)
root.geometry("%dx%d+%d+%d" % (width, height, x, y))
root.resizable(0, 0)


class ImageCompressor:
    def __init__(self, root):
        self.root = root
        self.root.configure(bg="#fff")
        self.db = sqlite3.connect("app.db") 
        self.create_table()
        

    def Show(self):
        self.clear_widgets()
        self.a = tk.Label(self.root, text="IMAGE COMPRESSOR", foreground="purple", width=30, font=("Times New Roman", 26))
        self.a.place(x=25, y=40)

        self.b = tk.Label(self.root, text="Click on upload to insert image", foreground="blue", width=40, font=("arial", 12))
        self.b.place(x=130, y=230)
        self.btn = tk.Button(self.root, text="Upload", width=10, bg="lightgreen", font=("arial", 20))
        self.btn.place(x=220, y=280)
        self.btn.bind("<Button>", lambda event: self.logic())

    def logic(self):
        self.file_path = askopenfilename()
        self.img = PIL.Image.open(self.file_path)
        self.myHeight, self.myWidth = self.img.size
        self.img = self.img.resize((self.myHeight, self.myWidth), PIL.Image.LANCZOS)
        self.save_path = asksaveasfilename()
        self.img.save(self.save_path + " compressed.JPG")

    def create_table(self):
        cursor = self.db.cursor()
        cursor.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='app'")
        if cursor.fetchone() is None:
            cursor.execute('''CREATE TABLE app (
                                email TEXT UNIQUE,
                                password TEXT)''')
            self.db.commit()    

    def show_login_page(self):
        self.clear_widgets()
        self.label_signup = tk.Label(self.root, text="Login", font=("Arial", 16), bg="skyblue", fg="#FFF")  
        self.label_signup.pack(pady=20)
        
        self.label_email = tk.Label(self.root, text="Email:", font=("Arial", 12), bg="#FFF")
        self.label_email.pack(pady=5)
        self.entry_email = tk.Entry(self.root, font=("Arial", 12))
        self.entry_email.pack(pady=5)

        self.label_password = tk.Label(self.root, text="Password:", font=("Arial", 12), bg="#FFF")
        self.label_password.pack(pady=5)
        self.entry_password = tk.Entry(self.root, show="*", font=("Arial", 12))
        self.entry_password.pack(pady=5)

        self.btn_login = tk.Button(self.root, text="Login", command=self.login, font=("Arial", 12), bg="skyblue", fg="#FFF") 
        self.btn_login.pack(pady=10)

       
        self.btn_signup = tk.Button(self.root, text="Signup", command=self.show_signup_page, font=("Arial", 12), bg="skyblue", fg="#FFF")  
        self.btn_signup.pack(pady=10)
        self.btn_back = tk.Button(self.root, text="←", command=self.show, font=("Arial", 12), bg="skyblue", fg="#FFF") 
        self.btn_back.pack(pady=10, padx=10, anchor="nw")

    def show_signup_page(self):

       
        self.clear_widgets()

        
        self.label_signup = tk.Label(self.root, text="Sign Up", font=("Arial", 16), bg="skyblue", fg="#FFF")  
        self.label_signup.pack(pady=20)

        self.label_email = tk.Label(self.root, text="Email:", font=("Arial", 12), bg="#FFF")
        self.label_email.pack(pady=5)
        self.entry_email = tk.Entry(self.root, font=("Arial", 12))
        self.entry_email.pack(pady=5)

        self.label_password = tk.Label(self.root, text="Password:", font=("Arial", 12), bg="#FFF")
        self.label_password.pack(pady=5)
        self.entry_password = tk.Entry(self.root, show="*", font=("Arial", 12))
        self.entry_password.pack(pady=5)

        self.btn_signup = tk.Button(self.root, text="Signup", command=self.signup, font=("Arial", 12), bg="skyblue", fg="#FFF")  
        self.btn_signup.pack(pady=10)

        
        self.btn_back = tk.Button(self.root, text="←", command=self.show_login_page, font=("Arial", 12), bg="skyblue", fg="#FFF") 
        self.btn_back.pack(pady=10, padx=10, anchor="nw")

    def login(self):
        email = self.entry_email.get()
        password = self.entry_password.get()
        if not self.validate_email(email) or not self.validate_password(password):
            messagebox.showerror("Login Failed", "Invalid email or password format")
            return
        if self.check_credentials(email, password):
            messagebox.showinfo("Login Successful", "You have logged in successfully!")
            self.Show()
        else:
            messagebox.showerror("Login Failed", "Invalid email or password")

    def signup(self):
        email = self.entry_email.get()
        password = self.entry_password.get()
        if not self.validate_email(email) or not self.validate_password(password):
            messagebox.showerror("Signup Failed", "Invalid email or password format")
        elif self.add_user(email, password):
            messagebox.showinfo("Signup Successful", "You have signed up successfully!")
            self.show_login_page()
        else:
            messagebox.showerror("Signup Failed", "Email already exists")

    def check_credentials(self, email, password):
        cursor = self.db.cursor()
        cursor.execute("SELECT * FROM app WHERE email=? AND password=?", (email, password))
        return cursor.fetchone() is not None

    def add_user(self, email, password):
        cursor = self.db.cursor()
        try:
            cursor.execute("INSERT INTO app (email, password) VALUES (?, ?)", (email, password))
            self.db.commit()
            return True
        except sqlite3.IntegrityError:
            return False        

    def validate_email(self, email):
        email_pattern = re.compile(r"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$")
        return bool(email_pattern.match(email))

    def validate_password(self, password):
        return len(password) >= 8

    
    def Menu(self):
        btn1 = Button(root, text="👤", width=2,height=1, foreground="black", font=("arial", 20))
        btn1.place(x=10, y=30)
        btn1.bind("<Button>", lambda event: self.show_login_page())
    def clear_widgets(self):
        for widget in self.root.winfo_children():
            widget.destroy()
    




app = ImageCompressor(root)
app.Show()
app.Menu()
root.mainloop()

